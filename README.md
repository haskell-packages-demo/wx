# wx

Portable and native GUI library for Haskell built on top of wxWidgets. [wx](https://hackage.haskell.org/package/wx)

* [WxHaskell](https://wiki.haskell.org/WxHaskell)
  wiki.haskell.org
  * [Quick_start](https://wiki.haskell.org/WxHaskell/Quick_start)
* https://en.wikibooks.org/wiki/Haskell/GUI
* [reactive-banana-wx](https://hackage.haskell.org/package/reactive-banana-wx)
